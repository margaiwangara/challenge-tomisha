import { IState } from "./types";

export const state = () => ({
  activeTab: 0,
});

export const mutations = {
  setActiveTab(state: IState, tab: number) {
    state.activeTab = tab;
  },
};
