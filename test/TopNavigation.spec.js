import { mount } from "@vue/test-utils";
import TopNavigation from "@/components/TopNavigation.vue";

describe("TopNavigation", () => {
  test("is a Vue instance", () => {
    const wrapper = mount(TopNavigation);
    expect(wrapper.vm).toBeTruthy();
  });

  test("has Login text", () => {
    const wrapper = mount(TopNavigation);
    expect(wrapper.text()).toBe("Login");
  });
});
