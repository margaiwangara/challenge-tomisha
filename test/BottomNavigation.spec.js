import { mount } from "@vue/test-utils";
import BottomNavigation from "@/components/BottomNavigation.vue";

describe("BottomNavigation", () => {
  test("is a Vue instance", () => {
    const wrapper = mount(BottomNavigation);
    expect(wrapper.vm).toBeTruthy();
  });

  test("has a button with text", () => {
    const wrapper = mount(BottomNavigation);

    const button = wrapper.find("button");
    expect(button.text()).toBe("Kostenlos Registrieren");
  });
});
