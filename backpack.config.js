const OpenBrowserPlugin = require("open-browser-webpack-plugin");
module.exports = {
  webpack: (config, options, webpack) => {
    config.entry.main = "./server/index.js";
    const openBrowser = new OpenBrowserPlugin({
      url: "http://localhsot:3000",
      delay: 20 * 1000,
    });
    config.plugins.push(openBrowser);
    return config;
  },
};
